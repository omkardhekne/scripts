-- ---------------------------------------------------------------------------
-- Inuxu Digital Media Technologies Pvt. Ltd.
-- ---------------------------------------------------------------------------
-- Name.......: deletedata.sql
-- Author.....: Omkar Dhekne <omkar@inuxu.media>
-- Editor.....: Omkar Dhekne <omkar@inuxu.media>
-- Date.......: 2021-05-16
-- Revision...: v1.0
-- Purpose....: For deleting data from old conversiondetailsummary monthwise.
-- Notes......: --
-- Reference..: Based on requirement from : Conversion report data summary and QA Department.
-- ---------------------------------------------------------------------------


CREATE PROCEDURE adgebra_summary.conversiondetailsummary_deletedata(
	IN monthfrom	VARCHAR(255),
    IN monthto      VARCHAR(255)
)

BEGIN
    --  CHECK AND DISPLAY NUMBER OF ROWS FOR INPUT MONTHS IN OLD CONVERSIONDETAILSUMMARY.    
    SELECT COUNT(*) as 'NUMBER OF ROWS TO BE DELETED FROM : OLD CONVERSIONDETAILSUMMARY TABLE'
	FROM adgebra_summary.conversiondetailsummary
	WHERE monthid BETWEEN monthfrom AND monthto;

    -- DELETE DATA FROM CONVERSIONDETAILSUMMARY.
    DELETE FROM adgebra_summary.conversiondetailsummary
    WHERE
        monthid BETWEEN monthfrom AND monthto;

    -- CHECK IF DATA FOR INPUT MONTHS HAVE DELETED FROM FROM OLD CONVERSIONDETAILSUMMARY.    
    SELECT COUNT(*) as 'CHECK IF DATA FOR INPUT MONTHS HAVE DELETED FROM : OLD CONVERSIONDETAILSUMMARY TABLE'
	FROM adgebra_summary.conversiondetailsummary
	WHERE monthid BETWEEN monthfrom AND monthto;

END;

-- CALL PROCEDURE.
-- CALL adgebra_summary.conversiondetailsummary_deletedata('2103','2104');
