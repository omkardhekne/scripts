-- ---------------------------------------------------------------------------
-- Inuxu Digital Media Technologies Pvt. Ltd.
-- ---------------------------------------------------------------------------
-- Name.......: importdata.sql
-- Author.....: Omkar Dhekne <omkar@inuxu.media>
-- Editor.....: Omkar Dhekne <omkar@inuxu.media>
-- Date.......: 2021-05-13
-- Revision...: v1.0
-- Purpose....: For importing data from conversiondetailsummary to newly created conversiondetailsummary_new monthwise.
-- Notes......: --
-- Reference..: Based on requirement from : Conversion report data summary and QA Department.
-- ---------------------------------------------------------------------------


CREATE PROCEDURE adgebra_summary.conversiondetailsummary_importdata(
	IN MONTHFROM	VARCHAR(255),
    IN MONTHTO      VARCHAR(255)
)

BEGIN
    -- INSERT DATA FROM CONVERSIONDETAILSUMMARY TO CONVERSIONDETAILSUMMARY_NEW.
    INSERT INTO adgebra_summary.conversiondetailsummary_new
    (
        timestamp,
        accountid,
        advertiserid,
        siteid,
        partnerid,
        campaignid,
        campaigntypeid,
        campaignmanagerid,
        deliverychannelid,
        bannerid,
        bannertype,
        bannersize,
        productids,
        cartvalue,
        customparameters,
        orderid,
        hrid,
        dayid,
        monthid,
        conversiontype,
        tscreated,
        ipaddress,
        postbackflag,
        useragent,
        acceptheader,
        referurl
    )
    SELECT
        timestamp,
        accountid,
        advertiserid,
        siteid,
        partnerid,
        campaignid,
        campaigntypeid,
        campaignmanagerid,
        deliverychannelid,
        bannerid,
        bannertype,
        bannersize,
        productids,
        cartvalue,
        customparameters,
        orderid,
        hrid,
        dayid,
        monthid,
        conversiontype,
        tscreated,
        ipaddress,
        postbackflag,
        useragent,
        acceptheader,
        referurl 
    FROM
        adgebra_summary.conversiondetailsummary 
    WHERE
        monthid BETWEEN monthfrom AND monthto;

    -- DISPLAY NUMBER OF ROWS INSERTED IN CONVERSIONDETAILSUMMARY_NEW.    
    SELECT COUNT(*) as 'NUMBER OF ROWS INSERTED IN : CONVERSIONDETAILSUMMARY_NEW TABLE'
	FROM adgebra_summary.conversiondetailsummary_new
	WHERE monthid BETWEEN monthfrom AND monthto;

END;

-- CALL PROCEDURE.
-- CALL adgebra_summary.conversiondetailsummary_importdata('2103','2104');
