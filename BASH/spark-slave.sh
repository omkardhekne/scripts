#!/bin/bash
#----------------------------------------------------------------------------
# Inuxu Digital Media Technologies Pvt. Ltd.
# ---------------------------------------------------------------------------
# Name.......: spark-slave.sh
# Author.....: Omkar Dhekne <omkar@inuxu.media>
# Date.......: 2021.06.01
# Purpose....: Script to Start / Stop / Check status of spark-slave services.
# Reference..: This script is created from Infra google drive document: start killed hadoop-spark nodes/service.
# Revision...: v1.0
# ---------------------------------------------------------------------------

# Environment variables:    
    # Script Revision:
        REV="v1.0"

    # Common:
        HADOOP='/usr/local/hadoop/current/sbin/hadoop-daemon.sh'
        YARN='/usr/local/hadoop/current/sbin/yarn-daemon.sh'
        STARTSLAVE='/usr/local/spark/current/sbin/start-slave.sh'
        STOPSLAVE='/usr/local/spark/current/sbin/stop-slave.sh'

# Functions:
    # Start spark-slave
    start()
    {
        echo -e "Starting datanode:"
        ${HADOOP} start datanode

        echo -e "\nStarting nodemanager:"
        ${YARN} start nodemanager

        echo -e "\nStarting spark slave:"
        ${STARTSLAVE} spark://spark-master:7077
    }

    # Stop spark-slave
    stop()
    {
        echo -e "Stopping datanode:"
        ${HADOOP} stop datanode

        echo -e "\nStopping nodemanager:"
        ${YARN} stop nodemanager

        echo -e "\nStopping spark slave:"
        ${STOPSLAVE}

    }

    # Status of spark-slave
    status()
    {
        jps
    }

    # Print help
    help()
    {
        cat <<EOF
    +------------------------------------------+
    +      Spark-slave Utility     : $REV      +
    +------------------------------------------+

    $0 start             :   Start spark-slave.
    $0 stop              :   Stop spark-slave.
    $0 status            :   Check status of spark-slave.
    $0 help              :   Print this help.
EOF
    }


# Execute functions:
    # If no commandline argument is passed then print help.
    if [[ -z $1 ]]
    then
        help
    else
        $1
    fi