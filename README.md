I have created these scripts to optimize server operations at Inuxu Digital Media Technologies Pvt. Ltd. infrastructure

1. [importdata.sql](https://bitbucket.org/omkardhekne/scripts/raw/84400e92b67deaa9ea3fcd7d4c5212e8aa4b690a/SQL/importdata.sql) : This file contains stored procedure for importing data from (old) conversiondetailsummary to conversiondetailsummary_new table monthwise.
2. [deletedata.sql](https://bitbucket.org/omkardhekne/scripts/raw/84400e92b67deaa9ea3fcd7d4c5212e8aa4b690a/SQL/importdata.sql) : This file contains stored procedure for deleting data from (old) conversiondetailsummary monthwise.
3. [spark-slave.sh](https://bitbucket.org/omkardhekne/scripts/raw/84400e92b67deaa9ea3fcd7d4c5212e8aa4b690a/BASH/spark-slave.sh) : Script to Start / Stop / Check status of spark-slave services.
4. [spark-server.sh](https://bitbucket.org/omkardhekne/scripts/raw/7349fa084d2c96f548871653b16062cc22e3a333/BASH/spark-server.sh) : Script to Start / Stop / Check status of spark-master services..
5. [spark-data-check.sh](https://bitbucket.org/omkardhekne/scripts/raw/696434aadef23f9eef6f5e4102241287e36ad998/BASH/spark-data-check.sh) : Script to check/email data from mysql and spark for data_import.sh script.


Current file structure:

<pre>
    .
    ├── BASH
    │   ├── spark-data-check.sh
    │   ├── spark-server.sh
    │   └── spark-slave.sh
    ├── README.md
    └── SQL
        ├── deletedata.sql
        └── importdata.sql
</pre>

2 directories, 6 files
