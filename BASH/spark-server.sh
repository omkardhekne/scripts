#!/bin/bash
#----------------------------------------------------------------------------
# Inuxu Digital Media Technologies Pvt. Ltd.
# ---------------------------------------------------------------------------
# Name.......: spark-master.sh
# Author.....: Omkar Dhekne <omkar@inuxu.media>
# Date.......: 2021.06.01
# Purpose....: Script to Start / Stop / Check status of spark-master services.
# Reference..: This script is created from Infra google drive document: Hadoop / Spark service restart.
# Revision...: v1.0
# ---------------------------------------------------------------------------

# Environment variables:    
    # Script Revision:
        REV="v1.0"

# Functions:
    # Start spark-slave
    start()
    {
        echo -e "Starting HADOOP Server:"
        /usr/local/hadoop/current/sbin/start-all.sh

        echo -e "\nStarting Spark Server:"
        /usr/local/spark/current/sbin/start-all.sh

        echo -e "\nStarting Thrift Server:"
        /usr/local/spark/current/sbin/start-thriftserver.sh --hiveconf hive.server2.thrift.port=61080  --num-executors 30 --executor-cores 4 --executor-memory 4G --conf spark.scheduler.mode=FAIR
    }

    # Stop spark-slave
    stop()
    {
        echo -e "\nStopping Thrift Server:"
        /usr/local/spark/current/sbin/stop-thriftserver.sh

        echo -e "\nStopping Spark Server:"
        /usr/local/spark/current/sbin/stop-all.sh

        echo -e "Stopping HADOOP Server:"
        /usr/local/hadoop/current/sbin/stop-all.sh
    }

    # Status of spark-slave
    status()
    {
        jps
    }

    # Print help
    help()
    {
        cat <<EOF
    +------------------------------------------+
    +      Spark-server Utility     : $REV     +
    +------------------------------------------+

    $0 start             :   Start spark-master.
    $0 stop              :   Stop spark-master.
    $0 status            :   Check status of spark-master.
    $0 help              :   Print this help.
EOF
    }


# Execute functions:
    # If no commandline argument is passed then print help.
    if [[ -z $1 ]]
    then
        help
    else
        $1
    fi